import argparse

import settings
from datetime import date

class Handler:
    def __init__(self):
        self.todo_file = settings.TODO_FILE
        self.done_file = settings.DONE_FILE

    def handle(self):
        """Interpret the first command line argument, and redirect."""
        parser = argparse.ArgumentParser()
        parser.add_argument(
            "action",
            choices=["add", "list", "delete", "do", "done"],
            help="The action to take",
        )
        parser.add_argument("other", nargs="?")
        parser.add_argument("--filter", dest="filter_str", default=None, help="Filter list by string")
        args = parser.parse_args()

        action = getattr(self, args.action)
        action()

    def list(self, filter_str=None):
        """Show all items in the todo file that contain the given string."""
        with open(self.todo_file) as f:
            items = f.readlines()
            num_items = len(items)
            digits_needed = len(str(num_items))
        for i, line in enumerate(items):
            if filter_str is not None and filter_str not in line:
                continue
            item_num = i + 1
            item_num_str = str(item_num).rjust(digits_needed)
            print(f"{item_num_str} {line.strip()}")
        print(f"---\n{num_items} item(s)")

    def done(self):
        """Show all items in the done file."""
        with open(self.done_file) as f:
            items = f.readlines()
            num_items = len(items)
            digits_needed = len(str(num_items))
        for i, line in enumerate(items):
            item_num = i + 1
            item_num_str = str(item_num).rjust(digits_needed)
            print(f"{item_num_str} {line.strip()}")
        print(f"---\n{len(items)} item(s) done")

    def add(self):
        """Add a new item to the todo file."""
        parser = argparse.ArgumentParser()
        parser.add_argument("action", choices=["add"])
        parser.add_argument("item", type=str)
        args = parser.parse_args()
        args_no_break = args.item.replace("\n", " ")
        with open(self.todo_file, "a") as f:
            f.write(args_no_break + "\n")

    def do(self):
        """Move an item from the todo file to the done file."""
        parser = argparse.ArgumentParser()
        parser.add_argument("action", choices=["do"])
        parser.add_argument("line_number", type=int)
        args = parser.parse_args()

        # Read in all the todo items
        with open(self.todo_file, "r") as f:
            items = f.readlines()

        # If todo list is empty
        if len(items) < 1:
            print(f"Your list is empty. Please add items first")
            return

        # Check if index is out of range
        if args.line_number < 1 or args.line_number >= len(items) + 1:
            print(f"There is no item {args.line_number}. Please choose a number from 0 to {len(items)-1}")
            return

        # Append the done item to the done file
        with open(self.done_file, "a") as f:
            i = args.line_number
            done_item = items[i-1].strip() + " (" + date.today().strftime("%Y-%m-%d") + ")"
            f.write(done_item + "\n")

        # Write out all but the done items
        with open(self.todo_file, "w") as f:
            new_todos = "".join(
                items[: args.line_number -1] + items[args.line_number :]
            )
            f.write(new_todos)

        print(f"Done: {items[args.line_number - 1].strip()}")

    def delete(self):
        parser = argparse.ArgumentParser()
        parser.add_argument("action", choices=["delete"])
        parser.add_argument("line_number", type=int)
        args = parser.parse_args()

        # Read in all the todo items
        with open(self.todo_file, "r") as f:
            items = f.readlines()

        # If todo list is empty
        if len(items) < 1:
            print(f"Your list is empty. Please add items first")
            return

        if args.line_number < 1 or args.line_number > len(items):
            print(f"Error: line number must be between 1 and {len(items)}.")
            return

        print(f"Deleted: {items[args.line_number - 1].strip()}")
        items.remove(items[args.line_number - 1])

        # Write out all but the deleted item
        with open(self.todo_file, "w") as f:
             f.write("".join(items))

        

if __name__ == "__main__":
    handler = Handler()
    handler.handle()
